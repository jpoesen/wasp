<?php
/*
  Plugin Name: Wasp Tools
  Plugin URI:
  Description: Does things.
  Version: 1.0
  Author: Joeri Poesen
  Author URI: https://swiftcircle.com
  License: GPLv2
*/
declare(strict_types=1);

namespace Wasp;

use Wasp;
use Wasp\Plugin;
use Wasp\PluginManager;

// Make the \Wasp object globally accessible.
if ( file_exists( __DIR__ . '/src/Wasp.php' ) ) {
	require_once __DIR__ . '/src/Wasp.php';
}

$plugin = new Plugin();
$manager = new PluginManager();
$manager->register($plugin);
