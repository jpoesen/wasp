<?php

declare(strict_types=1);

namespace Wasp;

use Wasp\PluginBase;


class Plugin extends PluginBase {

  public static function get_actions() {
    return [
      'wp_loaded' => 'on_plugins_loaded'
    ];
  }

  public function on_plugins_loaded(){
    // EventRegistrationHandler::submit();
  }

}