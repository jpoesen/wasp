<?php 

declare(strict_types=1);

namespace Wasp;

use Wasp\Subscriber\ActionHookSubscriberInterface;
use Wasp\Subscriber\FilterHookSubscriberInterface;


abstract class PluginBase implements ActionHookSubscriberInterface, FilterHookSubscriberInterface {

    public function __construct(){
    
    }


    public static function get_actions()
    {
        return [];
    }

    public static function get_filters()
    {
        return [];
    }
}