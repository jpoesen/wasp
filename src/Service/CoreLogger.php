<?php 
declare(strict_types=1);

namespace Wasp\Service;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class CoreLogger {

  public function hello(string $msg){
    die($msg);
  }

  public function log(string $msg, string $log_name = 'app') {
    $log = new Logger('CoreLogger');
    $log->pushHandler(new StreamHandler(ABSPATH . "/../../log/{$log_name}.log", Logger::DEBUG));
    
    // add records to the log
    $log->info($msg);
  }
}