<?php

declare(strict_types=1);

namespace Wasp\Form;


abstract class FormHandlerBase {

  protected static $form_data = [];

  protected static function getData(string $method = 'POST')
  {
    self::$form_data = ($method == 'POST') ? $_POST : $_GET;
  }

  // Example usage: field_is_present('form_id').
  protected static function field_is_present(string $element_name): bool {
    
    // Check if form element name is present.
    if (!isset(self::$form_data[$element_name])) {
      return false;
    }

    return true;
  }

    // Example usage: field_has_value('form_id', 'form-id-goes-here').
    protected static function field_has_value(string $element_name, string $element_value): bool {
    
      // Check if form element name is present.
      if (!isset(self::$form_data[$element_name])) {
        return false;
      }
  
      // Check if form element name matches provided value.
      if(self::$form_data[$element_name] != $element_value) {
        return false;
      }
  
      return true;
    }

}
