<?php
declare(strict_types=1);

use Symfony\Component\DependencyInjection\ContainerBuilder;


class Wasp {
    public static $container;

    /**
     * Build Service Container.
     *
     * @return ContainerBuilder
     */
    public static function buildContainer() {
        
        if (self::$container === null) {
            // Create a new ContainerBuilder instance.
            $container = new ContainerBuilder();
            
            // Define new services here.
            // $container->setDefinition('example_service', new Definition('ExampleClass'))
            //     ->addArgument('Example Argument');

            $container->register('logger', 'Wasp\Service\CoreLogger');
            


                // Set the new container.
            self::$container = $container;
        }

        return self::$container;
    }

    public static function service($service_id) {
      if (!self::$container) {
        self::buildContainer();
      }

      return self::$container->get($service_id);
  }
    
}