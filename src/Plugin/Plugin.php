<?php

declare(strict_types=1);

namespace Wasp;

use Wasp;
use Wasp\PluginBase;


class Plugin extends PluginBase {

  public function init_service_container(): void 
  {
    $wasp = new Wasp();
    dump('wasp inited');
  }

  public static function get_actions() 
  {
    return [
      // 'wp_loaded' => 'on_plugins_loaded'
    ];
  }

  public function on_plugins_loaded()
  {
  }

}